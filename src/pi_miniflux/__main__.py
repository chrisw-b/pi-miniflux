from typing import Optional
from PIL import Image, ImageFont, ImageDraw
from inky import InkyPHAT
from dotenv import load_dotenv
import os
from datetime import datetime
from io import BytesIO
import base64
import miniflux

load_dotenv()

USER_NAME = os.getenv("USERNAME")
API_KEY = os.getenv("API_KEY")
CATEGORY_ID = os.getenv("CATEGORY_ID")
MINIFLUX_HOST = os.getenv("MINIFLUX_HOST")

ICON_GUTTER_WIDTH = 40
SPACING = 4


inky_display = InkyPHAT("black")
inky_display.set_border(inky_display.WHITE)
font = ImageFont.truetype(
    "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf", 18
)


def wrap_text(text: str, width: int, font: ImageFont.FreeTypeFont) -> list[str]:
    text_lines = []
    text_line = []
    text = text.replace("\n", " [br] ")
    words = text.split()

    for word in words:
        if word == "[br]":
            text_lines.append(" ".join(text_line))
            text_line = []
            continue
        text_line.append(word)
        w = font.getlength(" ".join(text_line))
        if w + (SPACING * 2) > width:
            text_line.pop()
            text_lines.append(" ".join(text_line))
            text_line = [word]

    if len(text_line) > 0:
        text_lines.append(" ".join(text_line))

    return text_lines


def draw_text(draw: ImageDraw.ImageDraw, headline: str) -> None:
    GUTTER_WIDTH = ICON_GUTTER_WIDTH + (SPACING * 2)
    lines = wrap_text(headline, inky_display.WIDTH - GUTTER_WIDTH, font)
    y_text = 0

    for line in lines:
        _, top, __, bottom = font.getbbox(line)
        height = bottom - top
        draw.text((GUTTER_WIDTH, y_text), line, inky_display.BLACK, font)
        y_text += height + SPACING


def draw_favicon(img: Image.Image, encoded_favicon: str) -> None:
    favicon = Image.open(BytesIO(base64.b64decode(encoded_favicon)))
    #    bw = favicon.convert('LA').point(lambda x : 255 if x > 150 else 0, mode='1')
    #    bw.save('bw.png')
    large_fav = favicon.resize((ICON_GUTTER_WIDTH, ICON_GUTTER_WIDTH), Image.NEAREST)
    CENTERED_IMG = (inky_display.HEIGHT / 2) - (ICON_GUTTER_WIDTH / 2)
    img.paste(large_fav, (int(SPACING / 2), int(CENTERED_IMG)))


def draw_headline(headline: str, encoded_favicon: Optional[str]) -> None:
    img = Image.new("P", (inky_display.WIDTH, inky_display.HEIGHT))
    draw = ImageDraw.Draw(img)
    draw_text(draw, headline)
    if encoded_favicon:
        draw_favicon(img, encoded_favicon)
    inky_display.set_image(img)
    inky_display.show()


def main() -> None:
    if not MINIFLUX_HOST or not API_KEY:
        return None

    client = miniflux.Client(MINIFLUX_HOST, api_key=API_KEY)
    stories = client.get_entries(
        category_id=CATEGORY_ID,
        status=["read", "unread"],
        limit=1,
        order="published_at",
        direction="desc",
    ).get("entries")

    if stories:
        encoded_favicon = None
        recent_story = stories[0]
        icon_data = recent_story.get("feed").get("icon")
        icon = client.get_feed_icon(feed_id=icon_data.get("feed_id")).get("data")

        if icon:
            encoded_favicon = icon.split(",")[1]

        draw_headline(recent_story.get("title"), encoded_favicon)
        now = datetime.now()
        current_time = now.strftime("%m/%d/%Y, %H:%M:%S")
        print("Updated story!", current_time)


if __name__ == "__main__":
    main()
